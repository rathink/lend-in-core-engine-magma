package com.kuliza.lending.backoffice.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "bo_config_headers_variables_mapping", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "header_id", "variable_id" }) })
public class HeadersVariablesMapping extends BaseModel {

	@Column(nullable = false)
	private int headerVariableOrder;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "header_id")
	private Headers header;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "variable_id")
	private Variables variable;

	public HeadersVariablesMapping() {
		super();
		this.setIsDeleted(false);
	}

	public HeadersVariablesMapping(int headerVariableOrder, Headers header, Variables variable) {
		super();
		this.headerVariableOrder = headerVariableOrder;
		this.header = header;
		this.variable = variable;
		this.setIsDeleted(false);
	}

	public int getHeaderVariableOrder() {
		return headerVariableOrder;
	}

	public void setHeaderVariableOrder(int headerVariableOrder) {
		this.headerVariableOrder = headerVariableOrder;
	}

	public Headers getHeader() {
		return header;
	}

	public void setHeader(Headers header) {
		this.header = header;
	}

	public Variables getVariable() {
		return variable;
	}

	public void setVariable(Variables variable) {
		this.variable = variable;
	}

}
