package com.kuliza.lending.backoffice.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kuliza.lending.backoffice.models.Buckets;
import com.kuliza.lending.backoffice.models.Roles;

@Repository
public interface BucketsDao extends CrudRepository<Buckets, Long> {

	public Buckets findById(long id);

	public Buckets findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<Buckets> findByLabelAndRole(String label, String role);

	public List<Buckets> findByLabelAndRoleAndIsDeleted(String label, String role, boolean isDeleted);

	public Buckets findByBucketKeyAndIsDeleted(String key, boolean isDeleted);

	public Buckets findByBucketKeyAndRoleAndIsDeleted(String key, Roles role, boolean isDeleted);

}
