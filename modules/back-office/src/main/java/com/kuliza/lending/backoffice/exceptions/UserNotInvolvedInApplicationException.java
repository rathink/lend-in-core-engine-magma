package com.kuliza.lending.backoffice.exceptions;

public class UserNotInvolvedInApplicationException extends RuntimeException {

	public UserNotInvolvedInApplicationException() {
		super();
	}

	public UserNotInvolvedInApplicationException(String message) {
		super(message);
	}
}