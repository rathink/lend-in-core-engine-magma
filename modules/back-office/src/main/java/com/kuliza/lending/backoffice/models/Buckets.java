package com.kuliza.lending.backoffice.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "bo_config_buckets", uniqueConstraints = { @UniqueConstraint(columnNames = { "bucketKey", "role_id" }) })
public class Buckets extends BaseModel {

	@Column(nullable = false)
	private String label;

	@Column(nullable = false)
	private String bucketKey;

	@Column(nullable = false)
	private int bucketOrder;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "role_id")
	private Roles role;

	@OneToMany(cascade = CascadeType.MERGE, mappedBy = "bucket", orphanRemoval = true)
	@OrderBy("bucketVariableOrder ASC")
	private Set<BucketsVariablesMapping> bucketsVariablesMapping;

	public Buckets() {
		super();
		this.setIsDeleted(false);
	}

	public Buckets(String label, String bucketKey, int bucketOrder, Roles role) {
		super();
		this.label = label;
		this.bucketKey = bucketKey;
		this.bucketOrder = bucketOrder;
		this.role = role;
		this.setIsDeleted(false);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getBucketKey() {
		return bucketKey;
	}

	public void setBucketKey(String bucketKey) {
		this.bucketKey = bucketKey;
	}

	public int getBucketOrder() {
		return bucketOrder;
	}

	public void setBucketOrder(int bucketOrder) {
		this.bucketOrder = bucketOrder;
	}

	public Roles getRole() {
		return role;
	}

	public void setRole(Roles role) {
		this.role = role;
	}

	public Set<BucketsVariablesMapping> getBucketsVariablesMapping() {
		return bucketsVariablesMapping;
	}

}
