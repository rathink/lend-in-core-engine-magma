package com.kuliza.lending.backoffice.models;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "bo_config_variables", uniqueConstraints = { @UniqueConstraint(columnNames = { "mapping", "role_id" }) })
public class Variables extends BaseModel {

	@Column(nullable = false)
	private String label;

	@Column(nullable = false)
	private String mapping;

	@Column(columnDefinition = "tinyint(1) default 0")
	private boolean editable;

	@Column(columnDefinition = "tinyint(1) default 0")
	private boolean writable;

	@Column(nullable = false)
	@Lob
	private String meta;

	@OneToOne(cascade = CascadeType.MERGE, orphanRemoval = true)
	@JoinColumn(name = "role_id")
	private Roles role;

	@Column(nullable = false)
	private String type;

	@OneToMany(cascade = CascadeType.MERGE, mappedBy = "variable", orphanRemoval = true)
	@OrderBy("optionOrder ASC")
	private Set<Options> options;

	public Variables() {
		super();
		this.setIsDeleted(false);
	}

	public Variables(String label, String mapping, boolean editable, boolean writable, String meta, Roles role) {
		super();
		this.label = label;
		this.mapping = mapping;
		this.editable = editable;
		this.writable = writable;
		this.meta = meta;
		this.role = role;
	}

	public Variables(String label, String mapping, boolean editable, boolean writable, String meta, String type,
			Roles role) {
		super();
		this.label = label;
		this.mapping = mapping;
		this.editable = editable;
		this.writable = writable;
		this.meta = meta;
		this.type = type;
		this.role = role;
		this.setIsDeleted(false);
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getMapping() {
		return mapping;
	}

	public void setMapping(String mapping) {
		this.mapping = mapping;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public boolean isWritable() {
		return writable;
	}

	public void setWritable(boolean writable) {
		this.writable = writable;
	}

	public String getMeta() {
		return meta;
	}

	public void setMeta(String meta) {
		this.meta = meta;
	}

	public Roles getRole() {
		return role;
	}

	public void setRole(Roles role) {
		this.role = role;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Set<Options> getOptions() {
		return options;
	}

	public void setOptions(Set<Options> options) {
		if (this.type.equalsIgnoreCase("dropDown"))
			this.options = options;
	}

}
