package com.kuliza.lending.backoffice.exceptions;

public class CommentsNotEnabledForRoleException extends RuntimeException {

	public CommentsNotEnabledForRoleException() {
		super();
	}

	public CommentsNotEnabledForRoleException(String message) {
		super(message);
	}
}
