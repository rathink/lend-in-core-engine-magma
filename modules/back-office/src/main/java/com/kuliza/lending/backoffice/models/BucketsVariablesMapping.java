package com.kuliza.lending.backoffice.models;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "bo_config_buckets_variables_mapping", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "bucket_id", "variable_id" }) })
public class BucketsVariablesMapping extends BaseModel {

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "bucket_id")
	private Buckets bucket;

	@ManyToOne(cascade = CascadeType.MERGE)
	@JoinColumn(name = "variable_id")
	private Variables variable;

	@Column(nullable = false)
	private int bucketVariableOrder;

	public BucketsVariablesMapping() {
		super();
		this.setIsDeleted(false);
	}

	public BucketsVariablesMapping(Buckets bucket, Variables variable, int bucketVariableOrder) {
		super();
		this.bucket = bucket;
		this.variable = variable;
		this.bucketVariableOrder = bucketVariableOrder;
		this.setIsDeleted(false);
	}

	public Buckets getBucket() {
		return bucket;
	}

	public void setBuckets(Buckets bucket) {
		this.bucket = bucket;
	}

	public Variables getVariable() {
		return variable;
	}

	public void setVariable(Variables variable) {
		this.variable = variable;
	}

	public int getBucketVariableOrder() {
		return bucketVariableOrder;
	}

	public void setBucketVariableOrder(int bucketVariableOrder) {
		this.bucketVariableOrder = bucketVariableOrder;
	}

	public void setBucket(Buckets bucket) {
		this.bucket = bucket;
	}

}
