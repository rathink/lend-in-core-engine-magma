package com.kuliza.lending.journey.service;

import java.util.Map;
import java.util.function.Function;

import org.flowable.engine.delegate.DelegateExecution;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;

import com.kuliza.lending.common.connection.HTTPConnection;
import com.kuliza.lending.common.pojo.HTTPResponse;
import com.kuliza.lending.engine_common.integration.BaseIntegration;

public class DummyIntegration extends BaseIntegration {

	@Override
	public void execute(DelegateExecution execution) {

	}

	public String getBase64EncodedLoanAgreement(String fileId) throws Exception {
		HTTPResponse response = HTTPConnection.sendGET("http", "129.213.118.91", 8002,
				"/download_file/?file_id=" + fileId + "&type=contract");
		return org.apache.commons.codec.binary.Base64
				.encodeBase64String(response.getResponse().toString().getBytes("UTF-8"));
	}

	public void sampleApiCall() {
		// c6b4df05-2a81-41e5-8480-3aa98a81b78a
		Function<Map<String, Object>, JSONObject> buildRequestPayload = new Function<Map<String, Object>, JSONObject>() {
			public JSONObject apply(Map<String, Object> dataToBuildRequestPayload) {
				JSONObject requestPayload = new JSONObject();
				try {
					requestPayload.put("document",
							getBase64EncodedLoanAgreement("c6b4df05-2a81-41e5-8480-3aa98a81b78a"));
				} catch (Exception e) {
				}
				requestPayload.put("document_name", "Loan_Agreement");
				requestPayload.put("document_extension", "PDF");
				requestPayload.put("email", "arpit.agrawal@kuliza.com");
				requestPayload.put("name", "Arpit");
				// TODO : Change Mobile Number to User Id
				requestPayload.put("client_user_id", "965965965");
				requestPayload.put("client_phone_number", "965965965");
				requestPayload.put("page_number", 4);
				requestPayload.put("x_position", 150);
				requestPayload.put("y_position", 400);
				return requestPayload;
			}
		};

		Function<HTTPResponse, Object> successCallback = new Function<HTTPResponse, Object>() {
			public Object apply(HTTPResponse response) {
				return "success";
			}
		};

		Function<HTTPResponse, Object> errorCallback = new Function<HTTPResponse, Object>() {
			public Object apply(HTTPResponse response) {
				return "error";
			}
		};
		try {
			callApi(null, null, HttpMethod.POST, "http", "129.213.53.246", 8000, "/api/docusign/", null,
					null, buildRequestPayload, successCallback, errorCallback);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
