package com.kuliza.lending.journey.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.joda.time.DateTime;

import com.kuliza.lending.common.model.BaseModel;

@Entity
@Table(name = "los_user_loan_data")

public class LOSUserLoanDataModel extends BaseModel {

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "losApplicationNumber", nullable = false)
	private LOSApplicationNumber losApplicationNumber;

	@Column(nullable = true)
	private String loanAmount;

	@Column(nullable = true)
	private int tenure;

	@Column(nullable = true)
	private String applicationDate;

	@Column(nullable = true)
	private String emi;

	@Column(nullable = true)
	private String dueDate;

	@Column(nullable = true)
	private String applicationStage;

	@Column(nullable = true)
	private String disbursementDate;

	@Column(nullable = true)
	private String disbursedAmount;

	@Column(nullable = true)
	private String disbursementChannel;

	@Column(nullable = true)
	private String disbursementStatus;

	@Column(nullable = true, unique = true)
	private String processInstanceId;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "userId", nullable = false)
	private LOSUserModel losUserModel;

	@Column(columnDefinition = "tinyint(1) default 0", nullable = false)
	private boolean applicationCompleted;

	@Column(nullable = true)
	private String currentJourneyStage;

	@Column(nullable = true)
	private String productCode;

	@Column(nullable = true)
	private String schemeId;

	@Column(nullable = true)
	private String loanEndDate;

	public LOSUserLoanDataModel() {
		super();
		this.setIsDeleted(false);
	}

	public LOSUserLoanDataModel(LOSApplicationNumber losApplicationNumber, String loanAmount, int tenure,
			String applicationDate, String emi, String dueDate, String applicationStage, String disbursementDate,
			String disbursedAmount, String disbursementChannel, String disbursementStatus, String processInstanceId,
			LOSUserModel losUserModel, boolean applicationCompleted, String currentJourneyStage, String productCode,
			String schemeId, String loanEndDate) {
		super();
		this.losApplicationNumber = losApplicationNumber;
		this.loanAmount = loanAmount;
		this.tenure = tenure;
		this.applicationDate = applicationDate;
		this.emi = emi;
		this.dueDate = dueDate;
		this.applicationStage = applicationStage;
		this.disbursementDate = disbursementDate;
		this.disbursedAmount = disbursedAmount;
		this.disbursementChannel = disbursementChannel;
		this.disbursementStatus = disbursementStatus;
		this.processInstanceId = processInstanceId;
		this.losUserModel = losUserModel;
		this.applicationCompleted = applicationCompleted;
		this.currentJourneyStage = currentJourneyStage;
		this.productCode = productCode;
		this.schemeId = schemeId;
		this.loanEndDate = loanEndDate;
	}

	public LOSApplicationNumber getLosApplicationNumber() {
		return losApplicationNumber;
	}

	public void setLosApplicationNumber(LOSApplicationNumber losApplicationNumber) {
		this.losApplicationNumber = losApplicationNumber;
	}

	public String getLoanAmount() {
		return loanAmount;
	}

	public void setLoanAmount(String loanAmount) {
		this.loanAmount = loanAmount;
	}

	public int getTenure() {
		return tenure;
	}

	public void setTenure(int tenure) {
		this.tenure = tenure;
	}

	public String getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(String applicationDate) {
		this.applicationDate = applicationDate;
	}

	public String getEmi() {
		return emi;
	}

	public void setEmi(String emi) {
		this.emi = emi;
	}

	public String getDueDate() {
		return dueDate;
	}

	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}

	public String getApplicationStage() {
		return applicationStage;
	}

	public void setApplicationStage(String applicationStage) {
		this.applicationStage = applicationStage;
	}

	public String getDisbursementDate() {
		return disbursementDate;
	}

	public void setDisbursementDate(String disbursementDate) {
		this.disbursementDate = disbursementDate;
	}

	public String getDisbursedAmount() {
		return disbursedAmount;
	}

	public void setDisbursedAmount(String disbursedAmount) {
		this.disbursedAmount = disbursedAmount;
	}

	public String getDisbursementChannel() {
		return disbursementChannel;
	}

	public void setDisbursementChannel(String disbursementChannel) {
		this.disbursementChannel = disbursementChannel;
	}

	public String getDisbursementStatus() {
		return disbursementStatus;
	}

	public void setDisbursementStatus(String disbursementStatus) {
		this.disbursementStatus = disbursementStatus;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public LOSUserModel getLosUserModel() {
		return losUserModel;
	}

	public void setLosUserModel(LOSUserModel losUserModel) {
		this.losUserModel = losUserModel;
	}

	public boolean isApplicationCompleted() {
		return applicationCompleted;
	}

	public void setApplicationCompleted(boolean applicationCompleted) {
		this.applicationCompleted = applicationCompleted;
	}

	public String getCurrentJourneyStage() {
		return currentJourneyStage;
	}

	public void setCurrentJourneyStage(String currentJourneyStage) {
		this.currentJourneyStage = currentJourneyStage;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getSchemeId() {
		return schemeId;
	}

	public void setSchemeId(String schemeId) {
		this.schemeId = schemeId;
	}

	public String getLoanEndDate() {
		return loanEndDate;
	}

	public void setLoanEndDate(String loanEndDate) {
		this.loanEndDate = loanEndDate;
	}

}
