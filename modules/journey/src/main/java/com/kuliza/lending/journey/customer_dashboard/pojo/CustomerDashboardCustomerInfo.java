package com.kuliza.lending.journey.customer_dashboard.pojo;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.journey.model.LOSUserModel;

public class CustomerDashboardCustomerInfo {

	private String name;
	private int age;
	private String dateOfBirth;
	private String sex;
	private String contactNumber;
	private String email;

	public CustomerDashboardCustomerInfo() {
		super();
		this.name = "";
		this.age = 0;
		this.dateOfBirth = "";
		this.sex = "";
		this.contactNumber = "";
		this.email = "";

	}

	public CustomerDashboardCustomerInfo(String name, int age, String dateOfBirth, String sex, String contactNumber,
			String email) {
		super();
		this.name = name;
		this.age = age;
		this.dateOfBirth = dateOfBirth;
		this.sex = sex;
		this.contactNumber = contactNumber;
		this.email = email;
	}

	public CustomerDashboardCustomerInfo(Map<String, Object> data) {
		super();
		this.name = CommonHelperFunctions.getStringValue(data.get(Constants.BORROWER_FULL_NAME_KEY));
		this.age = CommonHelperFunctions.getIntegerValue(data.get(Constants.APPLICANT_AGE));
		this.dateOfBirth = CommonHelperFunctions.getStringValue(data.get(Constants.DATE_OF_BIRTH_FROM_HYPERVERGE_KEY));
		this.sex = CommonHelperFunctions.getStringValue(data.get(Constants.GENDER_KEY));
		this.contactNumber = CommonHelperFunctions.getStringValue(data.get(Constants.MOBILE_NUMBER_KEY));
		this.email = CommonHelperFunctions.getStringValue(data.get(Constants.EMAIL_ID_KEY));

	}

	public CustomerDashboardCustomerInfo(LOSUserModel losUserModel) {
		super();
		if (losUserModel != null) {

			this.name = CommonHelperFunctions.getStringValue(losUserModel.getName());
			this.age = CommonHelperFunctions.getIntegerValue(losUserModel.getAge());
			this.dateOfBirth = CommonHelperFunctions.getStringValue(losUserModel.getDateOfBirth());
			this.sex = CommonHelperFunctions.getStringValue(losUserModel.getSex());
			this.contactNumber = CommonHelperFunctions.getStringValue(losUserModel.getContactNumber());
			this.email = CommonHelperFunctions.getStringValue(losUserModel.getEmail());

		} else {
			this.name = "";
			this.age = 0;
			this.dateOfBirth = "";
			this.sex = "";
			this.contactNumber = "";
			this.email = "";
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonIgnore
	public int getAge() {
		return age;
	}

	@JsonIgnore
	public void setAge(int age) {
		this.age = age;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	@JsonIgnore
	public String getSex() {
		return sex;
	}

	@JsonIgnore
	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getContactNumber() {
		return contactNumber;
	}

	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
