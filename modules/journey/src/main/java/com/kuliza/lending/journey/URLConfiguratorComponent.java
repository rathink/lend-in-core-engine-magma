package com.kuliza.lending.journey;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class URLConfiguratorComponent {

	@Value("${startServiceNameSymbol}")
	private String startServiceNameSymbol;

	@Value("${endServiceNameSymbol}")
	private String endServiceNameSymbol;

	@Value("${DMSServiceName}")
	private String DMSServiceName;

	@Value("${dmsIpAddress}")
	private String dmsIpAddress;

	@Value("${dmsPort}")
	private int dmsPort;

	public String getStartServiceNameSymbol() {
		return startServiceNameSymbol;
	}

	public void setStartServiceNameSymbol(String startServiceNameSymbol) {
		this.startServiceNameSymbol = startServiceNameSymbol;
	}

	public String getEndServiceNameSymbol() {
		return endServiceNameSymbol;
	}

	public void setEndServiceNameSymbol(String endServiceNameSymbol) {
		this.endServiceNameSymbol = endServiceNameSymbol;
	}

	public String getDMSServiceName() {
		return DMSServiceName;
	}

	public void setDMSServiceName(String dMSServiceName) {
		DMSServiceName = dMSServiceName;
	}

	public String getDmsIpAddress() {
		return dmsIpAddress;
	}

	public void setDmsIpAddress(String dmsIpAddress) {
		this.dmsIpAddress = dmsIpAddress;
	}

	public int getDmsPort() {
		return dmsPort;
	}

	public void setDmsPort(int dmsPort) {
		this.dmsPort = dmsPort;
	}

}
