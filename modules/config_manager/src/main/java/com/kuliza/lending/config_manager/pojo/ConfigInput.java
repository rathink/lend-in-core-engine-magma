package com.kuliza.lending.config_manager.pojo;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ConfigInput {

	@NotNull
	@Size(min=5, message="clientId should have atleast 5 characters")
	String clientId;
	
	@NotNull
	String jsonString;

	public ConfigInput() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ConfigInput(String clientId, String jsonString) {
		super();
		this.clientId = clientId;
		this.jsonString = jsonString;
	}

	public String getClientId() {
		return clientId;
	}

	public void setClientId(String clientId) {
		this.clientId = clientId;
	}

	public String getJsonString() {
		return jsonString;
	}

	public void setJsonString(String jsonString) {
		this.jsonString = jsonString;
	}

}
