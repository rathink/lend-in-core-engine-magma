package com.kuliza.lending.configurator.pojo;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class GetRequiredVariables {

	@NotNull(message = "productIds is a required key")
	List<Long> productIds;

	String userId;

	public GetRequiredVariables() {
		productIds = new ArrayList<>();
	}

	public GetRequiredVariables(List<Long> productIds) {
		super();
		this.productIds = productIds;
	}

	public List<Long> getProductIds() {
		return productIds;
	}

	public void setProductIds(List<Long> productIds) {
		this.productIds = productIds;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
