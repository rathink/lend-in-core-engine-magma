package com.kuliza.lending.configurator.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.SubmitNewProduct;
import com.kuliza.lending.configurator.pojo.UpdateProduct;
import com.kuliza.lending.configurator.service.productservice.ProductServices;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.validators.ProductDataValidator;

@RestController
@RequestMapping(value = "/api/product")
public class ProductControllers {

	private static final Logger logger = LoggerFactory.getLogger(ProductControllers.class);

	@Autowired
	private ProductServices productServices;

	@Autowired
	private ProductDataValidator productDataValidator;

	// API to get all Product Categories
	@RequestMapping(method = RequestMethod.GET, value = "/categories")
	public Object getProductCategories(HttpServletRequest request) {
		logger.info("--> Entering get product Categories()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			logger.debug("getting all product Categories :: userId is :" + userId);
			GenericAPIResponse response = productServices.getProductCategories();
			if (response.getStatus() == 200) {

				productServices.logSuccessResponse(request, userId, response);
			} else {
				productServices.logErrorResponse(request, userId, response);
			}
			logger.info("<-- Exiting get product Categories()");
			return response;
		} catch (Exception e) {
			return productServices.handleException(e, request, userId);
		}
	}

	// API to get all Products for user
	@RequestMapping(method = RequestMethod.GET, value = "/list")
	public Object getProductsList(@RequestParam(value = "productCategoryId", required = false) String productCategoryId,
			@RequestParam(value = "status", required = false, defaultValue = "0") String status,
			HttpServletRequest request) {
		logger.info("--> Entering get ProductList()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			response = productDataValidator.validateGetProductList(productCategoryId, status);
			if (response == null) {
				logger.debug(
						"getting product list for userId: " + userId + ":: productCategoryId: " + productCategoryId);
				response = productServices.getProductsList(userId, productCategoryId, status);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully fetched productList");
				productServices.logSuccessResponse(request, userId, response);
			} else {
				productServices.logErrorResponse(request, userId, response);
			}
			logger.info("<--Exiting get ProductList()");
			return response;
		} catch (Exception e) {
			return productServices.handleException(e, request, userId);
		}
	}

	// API to get data of all Products of user
	@RequestMapping(method = RequestMethod.GET, value = "")
	public Object getProducts(HttpServletRequest request) {
		logger.info("--> Entering get Products()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			logger.debug("getting product list for userId: " + userId);
			GenericAPIResponse response = productServices.getAllProductsData(userId);
			if (response.getStatus() == 200) {
				productServices.logSuccessResponse(request, userId, response);
			} else {
				productServices.logErrorResponse(request, userId, response);
			}
			logger.info("<-- Exiting get Products()");
			return response;
		} catch (Exception e) {
			return productServices.handleException(e, request, userId);
		}
	}

	// API to get data of single Product of user
	@RequestMapping(method = RequestMethod.GET, value = "/{productId:^[1-9]+[0-9]*$}")
	public Object getProduct(@PathVariable(value = "productId") String productId, HttpServletRequest request) {
		logger.info("--> Entering get product()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			if (!productDataValidator.validateProductId(productId, userId)) {
				logger.error("Invalid productId: " + productId);
				response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
						Constants.INVALID_PRODUCT_ID_MESSAGE);
			} else {
				logger.debug("get data of single Product of user:" + userId + ":: productId:" + productId);
				response = productServices.getSingleProduct(productId);
			}
			if (response.getStatus() == 200) {
				logger.debug("SuccessFully fetched data for product::" + response.getData());
				productServices.logSuccessResponse(request, userId, response);
			} else {
				productServices.logErrorResponse(request, userId, response);
			}
			logger.info("<--Exiting get product() ");
			return response;
		} catch (Exception e) {
			return productServices.handleException(e, request, userId);
		}

	}

	// API to create new Product for user
	@RequestMapping(method = RequestMethod.POST, value = "")
	public Object productSubmit(@Valid @RequestBody SubmitNewProduct input, BindingResult result,
			HttpServletRequest request) {
		logger.info("--> Entering product Submit()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			input.setUserId(userId);
			productDataValidator.validateNewProduct(input, result);
			response = productServices.checkErrors(result);
			if (response == null) {
				logger.debug("creating new product for user with ProductName:" + input.getProductName());
				response = productServices.createNewProduct(input);
			}
			if (response.getStatus() == 200) {
				logger.debug("SuccessFully created product for user");
				productServices.logSuccessResponse(request, userId, response, input);
			} else {
				productServices.logErrorResponse(request, userId, response, input);
			}
			logger.info("<-- Exiting product Submit()");
			return response;
		} catch (Exception e) {
			return productServices.handleException(e, request, userId, input);
		}
	}

	// API to create new Product Version for user
	@RequestMapping(method = RequestMethod.POST, value = "/{productId:^[1-9]+[0-9]*$}")
	public Object productCloning(@PathVariable(value = "productId") String productId, HttpServletRequest request) {
		logger.info("--> Entering product Cloning()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			response = productDataValidator.validateCloneProduct(productId, userId);
			if (response == null) {
				logger.debug("creating new product version for user");
				response = productServices.cloneProduct(userId, productId);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully created new product version for user");
				productServices.logSuccessResponse(request, userId, response);
			} else {
				productServices.logErrorResponse(request, userId, response);
			}
			logger.info("<-- Exiting product Cloning()");
			return response;
		} catch (Exception e) {
			return productServices.handleException(e, request, userId);
		}
	}

	// API to update Product Name
	@RequestMapping(method = RequestMethod.PUT, value = "/{productId:^[1-9]+[0-9]*$}")
	public Object updateProduct(@PathVariable(value = "productId") String productId,
			@Valid @RequestBody UpdateProduct input, BindingResult result, HttpServletRequest request) {
		logger.info("-->Entering update product()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			input.setProductId(productId);
			input.setUserId(userId);
			productDataValidator.validateUpdateProduct(input, result);
			response = productServices.checkErrors(result);
			if (response == null) {
				logger.debug("Updating product Name to: " + input.getProductName());
				response = productServices.updateProduct(input);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully updated product name");
				productServices.logSuccessResponse(request, userId, response, input);
			} else {
				productServices.logErrorResponse(request, userId, response, input);
			}
			logger.info("<-- Exiting update product()");
			return response;
		} catch (Exception e) {
			return productServices.handleException(e, request, userId, input);
		}
	}

	// API to Make Published Product Editable
	@RequestMapping(method = RequestMethod.PUT, value = "/{productId:^[1-9]+[0-9]*$}/edit")
	public Object editProduct(@PathVariable(value = "productId") String productId, HttpServletRequest request) {
		logger.info("--> Entering edit product()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			response = productDataValidator.validateEditProduct(productId, userId);
			if (response == null) {
				logger.debug("<-----Editing published product----->");
				response = productServices.editProduct(productId);
			}
			if (response.getStatus() == 200) {
				logger.debug("successfully edited product with Id:" + productId);
				productServices.logSuccessResponse(request, userId, response);
			} else {
				productServices.logErrorResponse(request, userId, response);
			}
			logger.info("<-- Exiting edit product()");
			return response;
		} catch (Exception e) {
			return productServices.handleException(e, request, userId);
		}
	}

	// API to publish Product
	@RequestMapping(method = RequestMethod.PUT, value = "/{productId:^[1-9]+[0-9]*$}/publish")
	public Object publishProduct(@PathVariable(value = "productId") String productId, HttpServletRequest request) {
		logger.info("-->Entering publish product()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			response = productDataValidator.validatePublishProduct(productId, userId);
			if (response == null) {
				logger.debug("publishing product with productId:" + productId);
				response = productServices.publishProduct(productId);
			}
			if (response.getStatus() == 200) {
				logger.debug("Product published Successfully");
				productServices.logSuccessResponse(request, userId, response);
			} else {
				productServices.logErrorResponse(request, userId, response);
			}
			logger.info("<-- Exiting Publish product()");
			return response;
		} catch (Exception e) {
			return productServices.handleException(e, request, userId);
		}
	}

	// API to deploy product
	@RequestMapping(method = RequestMethod.PUT, value = "/{productId:^[1-9]+[0-9]*$}/deploy")
	public Object deployProduct(@PathVariable(value = "productId") String productId, HttpServletRequest request) {
		logger.info("--> Entering deploy product()");
		String userId = String.valueOf(request.getAttribute(Constants.USER_ID));
		try {
			GenericAPIResponse response = null;
			response = productDataValidator.validateDeployProduct(productId, userId);
			if (response == null) {
				logger.debug("deploying product for productId:" + productId);
				response = productServices.deployProduct(userId, productId);
			}
			if (response.getStatus() == 200) {
				logger.debug("Successfully deployed product");
				productServices.logSuccessResponse(request, userId, response);
			} else {
				productServices.logErrorResponse(request, userId, response);
			}
			logger.info("<--Exiting deploy Product()");
			return response;
		} catch (Exception e) {
			return productServices.handleException(e, request, userId);
		}
	}

}
