package com.kuliza.lending.configurator.validators;

import com.kuliza.lending.configurator.pojo.NewExpression;
import com.kuliza.lending.configurator.pojo.SubmitNewExpressions;
import com.kuliza.lending.configurator.pojo.UpdateExpression;
import com.kuliza.lending.configurator.utils.Constants;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
public class ExpressionDataValidator extends BasicDataValidator {

	public boolean validateGetProductExpressions(String userId, String productId) throws Exception {
		return !validateProductId(productId, userId) ? false : true;
	}

	public boolean validateGetGroupExpressions(String userId, String productId, String groupId) throws Exception {
		if (!validateProductId(productId, userId)) {
			return false;
		}
		return !validateGroupId(groupId, productId) ? false : true;
	}

	public void validateExpression(NewExpression obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		// check if expression is group or product.
		boolean flag = true;
		if (obj.getGroupId().equals("")) {
			flag = false;
		}
		boolean checkError = validateExpressionString(obj.getExpressionString(), flag, obj.getProductId(), errors);
		if (!checkError) {
			errors.rejectValue(Constants.EXPRESSION_STRING, Constants.INVALID_EXPRESSION_STRING,
					Constants.INVALID_EXPRESSION_MESSAGE);
		}
	}

	public void validateNewExpression(SubmitNewExpressions obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		validateProductGroup(obj.getUserId(), obj.getProductId(), obj.getGroupId(), errors);
	}

	public void validateUpdateExpression(UpdateExpression obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		validateProductGroup(obj.getUserId(), obj.getProductId(), obj.getGroupId(), errors);
		if (!errors.hasErrors() && !validateExpressionId(obj.getExpressionId(), obj.getGroupId(), obj.getProductId())) {
			errors.rejectValue(Constants.EXPRESSION_ID, Constants.INVALID_EXPRESSION_ID,
					Constants.INVALID_EXPRESSION_ID_MESSAGE);
		}
	}

}
