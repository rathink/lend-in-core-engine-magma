package com.kuliza.lending.configurator.pojo;

import com.kuliza.lending.configurator.utils.Constants;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.util.HashMap;
import java.util.Map;

public class ProductionFireRulesInput {

	@NotNull(message = "identifier is a required key")
	String identifier;

	@NotNull(message = "groupId is a required key")
	@Pattern(regexp = Constants.EMPTY_OR_ID_REGEX, message = Constants.INVALID_GROUP_ID_MESSAGE)
	String groupId;

	@NotNull(message = "processInstanceId is a required key")
	@Pattern(regexp = Constants.EMPTY_OR_ID_REGEX, message = Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE)
	String processInstanceId;

	@NotNull(message = "inputData is a required key")
	Map<String, Object> inputData;

	String userId;

	public ProductionFireRulesInput() {
		this.identifier = "";
		this.groupId = "";
		this.processInstanceId = "";
		this.inputData = new HashMap<>();
	}

	public ProductionFireRulesInput(String identifier, String groupId, String processInstanceId,
			Map<String, Object> inputData) {
		this.identifier = identifier;
		this.groupId = groupId;
		this.processInstanceId = processInstanceId;
		this.inputData = inputData;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public Map<String, Object> getInputData() {
		return inputData;
	}

	public void setInputData(Map<String, Object> inputData) {
		this.inputData = inputData;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	// @Override
	// public String toString() {
	// StringBuilder inputData = new StringBuilder();
	// inputData.append("{ ");
	// inputData.append("identifier : " + identifier + ", ");
	// inputData.append("groupId : " + groupId + ", ");
	// inputData.append("processInstanceId : " + processInstanceId + ", ");
	// inputData.append("inputData : " + inputData.toString());
	// inputData.append(" }");
	// return inputData.toString();
	//
	// }

}
