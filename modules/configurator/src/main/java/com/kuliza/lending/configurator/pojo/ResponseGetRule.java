package com.kuliza.lending.configurator.pojo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.kuliza.lending.configurator.models.Rule;
import com.kuliza.lending.configurator.serializers.BulkRuleSerializer;

import java.util.Set;

public class ResponseGetRule {

	@JsonSerialize(converter = BulkRuleSerializer.class)
	public Set<Rule> allSavedRules;

	public ResponseGetRule() {
		super();
	}

	public ResponseGetRule(Set<Rule> allSavedRules) {
		super();
		this.allSavedRules = allSavedRules;
	}

	public Set<Rule> getAllSavedRules() {
		return allSavedRules;
	}

	public void setAllSavedRules(Set<Rule> allSavedRules) {
		this.allSavedRules = allSavedRules;
	}

}
