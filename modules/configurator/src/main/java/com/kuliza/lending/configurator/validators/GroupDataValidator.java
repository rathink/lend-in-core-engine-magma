package com.kuliza.lending.configurator.validators;

import com.kuliza.lending.configurator.models.Group;
import com.kuliza.lending.configurator.models.GroupDao;
import com.kuliza.lending.configurator.models.ProductDao;
import com.kuliza.lending.configurator.pojo.SubmitNewGroup;
import com.kuliza.lending.configurator.pojo.UpdateGroup;
import com.kuliza.lending.configurator.utils.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

@Component
public class GroupDataValidator extends BasicDataValidator {

	@Autowired
	private GroupDao groupDao;

	@Autowired
	private ProductDao productDao;

	public boolean validateGetAllGroup(String userId, String productId) throws Exception {
		return !validateProductId(productId, userId) ? false : true;
	}

	public boolean validateGetSingleGroup(String userId, String productId, String groupId) throws Exception {
		if (!validateProductId(productId, userId)) {
			return false;
		}
		return !validateGroupId(groupId, productId) ? false : true;
	}

	public void validateNewGroup(SubmitNewGroup obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		if (!validateProductId(obj.getProductId(), obj.getUserId())) {
			errors.rejectValue(Constants.PRODUCT_ID, Constants.INVALID_PRODUCT_ID,
					Constants.INVALID_PRODUCT_ID_MESSAGE);
		} else {
			if (productDao.findByIdAndIsDeleted(Long.parseLong(obj.getProductId()), false).getStatus() > 0) {
				errors.rejectValue(Constants.PRODUCT_ID, Constants.INVALID_PRODUCT_ID,
						Constants.PRODUCT_NOT_EDITABLE_MESSAGE);
			} else {
				if (groupDao.findByNameAndProductIdAndIsDeleted(obj.getGroupName(), Long.parseLong(obj.getProductId()),
						false) != null) {
					errors.rejectValue(Constants.GROUP_NAME, Constants.INVALID_GROUP_NAME,
							Constants.GROUP_NAME_TAKEN_MESSAGE);
				}
			}
		}
	}

	public void validateUpdateGroup(UpdateGroup obj, Errors errors) throws Exception {
		if (errors.hasErrors()) {
			return;
		}
		validateProductGroup(obj.getUserId(), obj.getProductId(), obj.getGroupId(), errors);
		if (!errors.hasErrors()) {
			Group group = groupDao.findByNameAndProductIdAndIsDeleted(obj.getGroupName(),
					Long.parseLong(obj.getProductId()), false);
			if (group != null && !Long.toString(group.getId()).equals(obj.getGroupId())) {
				errors.rejectValue("groupName", "groupName.invalid", Constants.GROUP_NAME_TAKEN_MESSAGE);
			}
		}
	}

}
