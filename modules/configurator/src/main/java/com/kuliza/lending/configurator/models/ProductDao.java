package com.kuliza.lending.configurator.models;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ProductDao extends CrudRepository<Product, Long> {

	public Product findById(long id);

	public Product findByIdAndIsDeleted(long id, boolean isDeleted);

	public List<Product> findByUserId(long userId);

	public List<Product> findByUserIdAndIsDeleted(long userId, boolean isDeleted);

	public Product findByIdAndUserIdAndIsDeleted(long id, long userId, boolean isDeleted);

	public List<Product> findByUserIdAndProductCategoryIdAndIsDeleted(long userId, long productCategoryId,
                                                                      boolean isDeleted);

	public List<Product> findByProductCategoryId(long productCategoryId);

	public List<Product> findByUserIdAndIsDeletedAndStatusGreaterThanEqual(long userId, boolean isDeleted, int status);

	public List<Product> findByUserIdAndProductCategoryIdAndIsDeletedAndStatusGreaterThanEqual(long userId,
                                                                                               long productCategoryId, boolean isDeleted, int status);

	public Product findByNameAndUserId(String name, long userId);

	public Product findByNameAndUserIdAndIsDeleted(String name, long userId, boolean isDeleted);

	public List<Product> findByIdentifier(String identifier);

	public List<Product> findByIdentifierAndIsDeleted(String identifier, boolean isDeleted);

}
