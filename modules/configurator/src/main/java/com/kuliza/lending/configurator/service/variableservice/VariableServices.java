package com.kuliza.lending.configurator.service.variableservice;

import com.kuliza.lending.configurator.models.*;
import com.kuliza.lending.configurator.pojo.GenericAPIResponse;
import com.kuliza.lending.configurator.pojo.NewVariable;
import com.kuliza.lending.configurator.pojo.SubmitNewVariables;
import com.kuliza.lending.configurator.pojo.UpdateVariable;
import com.kuliza.lending.configurator.service.genericservice.GenericServicesCE;
import com.kuliza.lending.configurator.utils.Constants;
import com.kuliza.lending.configurator.utils.HelperFunctions;
import com.kuliza.lending.configurator.validators.VariableDataValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.sql.Timestamp;
import java.util.*;

@Service
public class VariableServices extends GenericServicesCE {

	@Autowired
	private GroupDao groupDao;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private RuleDao ruleDao;

	@Autowired
	private VariableDao variableDao;

	@Autowired
	private ExpressionDao expressionDao;

	@Autowired
	private VariableDataValidator variableDataValidator;

	public GenericAPIResponse validateSubmitVariables(SubmitNewVariables input, BindingResult result, String userId,
                                                      String productId) throws Exception {
		Map<String, Object> data = new HashMap<>();
		GenericAPIResponse response = null;
		input.setUserId(userId);
		input.setProductId(productId);
		variableDataValidator.validateNewVariable(input, result);
		response = checkErrors(result);
		Boolean isError = false;
		if (response == null) {
			List<NewVariable> variables = input.getNewVariablesList();
			List<Map<String, Object>> variableErrors = new ArrayList<>();
			Set<String> variableNames = new HashSet<>();
			for (NewVariable variable : variables) {
				if (variableNames.contains(variable.getVariableName())) {
					Map<String, Object> variableError = new HashMap<>();
					List<FieldError> allFieldErrors = new ArrayList<>();
					FieldError fieldError = new FieldError("variable", Constants.VARIABLE_NAME,
							Constants.VARIABLE_NAME_TAKEN_MESSAGE);
					allFieldErrors.add(fieldError);
					variableError.put(Constants.VARIABLE_NAME, variable.getVariableName());
					variableError.put(Constants.DATA_ERRORS_KEY,
							HelperFunctions.generateErrorResponseData(allFieldErrors));
					variableErrors.add(variableError);
					isError = true;

				} else {
					variableNames.add(variable.getVariableName());
					BindingResult bindingResult = new BeanPropertyBindingResult(variable, "variable");
					variable.setProductId(productId);
					variableDataValidator.validateVariable(variable, bindingResult);
					if (bindingResult.hasErrors()) {
						Map<String, Object> variableError = new HashMap<>();
						variableError.put(Constants.VARIABLE_NAME, variable.getVariableName());
						variableError.put(Constants.DATA_ERRORS_KEY,
								HelperFunctions.generateErrorResponseData(bindingResult.getFieldErrors()));
						variableErrors.add(variableError);
						isError = true;
					} else {
						variableErrors.add(null);
					}
				}
			}
			if (isError) {
				data.put(Constants.DATA_ERRORS_KEY, variableErrors);
				response = new GenericAPIResponse(Constants.INVALID_POST_REQUEST_DATA_ERROR_CODE,
						Constants.FAILURE_MESSAGE, data);
			}
		}
		return response;
	}

	public GenericAPIResponse validateDeleteVariable(String userId, String productId, String variableId)
			throws Exception {
		GenericAPIResponse response = null;
		if (!variableDataValidator.validateGetSingleVariable(userId, productId, variableId)) {
			response = new GenericAPIResponse(Constants.NOT_FOUND_ERROR_CODE, Constants.FAILURE_MESSAGE,
					Constants.INVALID_PRODUCT_VARIABLE_ID_MESSAGE);
		} else if (productDao.findByIdAndIsDeleted(Long.parseLong(productId), false).getStatus() > 0) {
			response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
					Constants.PRODUCT_NOT_EDITABLE_MESSAGE);
		}
		if (response == null) {
			Variable variableToDelete = variableDao.findByIdAndIsDeleted(Long.parseLong(variableId), false);
			Product product = variableToDelete.getProduct();
			List<Group> allGroups = groupDao.findByProductIdAndIsDeleted(product.getId(), false);
			List<Variable> allDerivedVariables = variableDao.findByProductIdAndCategoryAndIsDeleted(product.getId(),
					Constants.DERIVED, false);
			// Array to store where the variable is used in ie which groups and
			// derived variables
			Set<String> variableUsedIn = new HashSet<>();
			for (Group group : allGroups) {
				List<Expression> allExpressions = expressionDao.findByGroupIdAndIsDeleted(group.getId(), false);
				for (Expression expression : allExpressions) {
					if (Arrays.asList(HelperFunctions
							.makeReadableExpressionString(expression.getExpressionString(), variableDao, groupDao, true)
							.split("\\s")).contains(variableToDelete.getName())) {
						variableUsedIn.add("Variable used in Expression of Group: " + group.getName());
					}
				}
				List<Rule> allRules = ruleDao.findByGroupIdAndIsDeleted(group.getId(), false);
				for (Rule rule : allRules) {
					if (rule.getVariable().getId() == Long.parseLong(variableId)) {
						variableUsedIn.add("Variable used in Rule of Group : " + group.getName());
					}
				}
			}

			for (Variable variable : allDerivedVariables) {
				if (Arrays.asList(HelperFunctions
						.makeReadableExpressionString(variable.getExpression(), variableDao, groupDao, true)
						.split("\\s")).contains(variableToDelete.getName())) {
					variableUsedIn.add("Variable used in Expression of Variable: " + variable.getName());
				}
			}

			if (!variableUsedIn.isEmpty()) {
				response = new GenericAPIResponse(Constants.REQUEST_FAILED_CODE, Constants.FAILURE_MESSAGE,
						variableUsedIn);
			}
		}
		return response;
	}

	public GenericAPIResponse getAllVariables(String productId) throws Exception {
		List<Variable> allProductVariables = variableDao.findByProductIdAndIsDeleted(Long.parseLong(productId), false);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, allProductVariables);
	}

	public GenericAPIResponse getSingleVariable(String variableId) throws Exception {
		Variable variable = variableDao.findByIdAndIsDeleted(Long.parseLong(variableId), false);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, variable);
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse createNewVariables(SubmitNewVariables input) throws Exception {
		Product product = productDao.findByIdAndIsDeleted(Long.parseLong(input.getProductId()), false);
		List<NewVariable> variables = input.getNewVariablesList();
		List<Variable> savedVariables = new ArrayList<>();
		for (NewVariable variable : variables) {
			Variable newVariable;
			if (variable.getVariableCategory().equals("Primitive")) {
				newVariable = new Variable(variable.getVariableName(), variable.getVariableType(),
						variable.getVariableSource(), variable.getVariableCategory(), variable.getVariableDescription(),
						variable.getExpressionString(), product);
			} else {
				String expressionString = HelperFunctions.makeStorableExpressionString(variable.getExpressionString(),
						variableDao, groupDao, input.getProductId(), true);
				newVariable = new Variable(variable.getVariableName(), variable.getVariableType(),
						variable.getVariableSource(), variable.getVariableCategory(), variable.getVariableDescription(),
						expressionString, product);
			}
			variableDao.save(newVariable);
			savedVariables.add(newVariable);
		}
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, savedVariables);
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse updateVariable(UpdateVariable input) throws Exception {
		Variable updateVariable = variableDao.findByIdAndIsDeleted(Long.parseLong(input.getVariableId()), false);
		updateVariable.setSource(input.getVariableSource());
		updateVariable.setDescription(input.getVariableDescription());
		if (!input.getExpressionString().equals("")) {
			String expressionString = HelperFunctions.makeStorableExpressionString(input.getExpressionString(),
					variableDao, groupDao, input.getProductId(), true);
			updateVariable.setExpression(expressionString);
		}
		updateVariable.setModified(new Timestamp(new Date().getTime()));
		variableDao.save(updateVariable);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE, updateVariable);
	}

	@Transactional(rollbackFor = Exception.class)
	public GenericAPIResponse deleteVariable(String variableId) throws Exception {
		Variable variableToDelete = variableDao.findByIdAndIsDeleted(Long.parseLong(variableId), false);
		// timestamp added to name so that later on, user can again
		// create or update a variable by same name as deleted one
		variableToDelete.setName(variableToDelete.getName().concat(Long.toString(new Date().getTime())));
		variableToDelete.setModified(new Timestamp(new Date().getTime()));
		variableToDelete.setIsDeleted(true);
		variableDao.save(variableToDelete);
		return new GenericAPIResponse(Constants.SUCCESS_STATUS_CODE, Constants.SUCCESS_MESSAGE,
				Constants.VARIABLE_DELETED_SUCCESS_MESSAGE);
	}

}
