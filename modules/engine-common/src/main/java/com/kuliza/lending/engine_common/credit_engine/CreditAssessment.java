package com.kuliza.lending.engine_common.credit_engine;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.flowable.engine.delegate.DelegateExecution;
import org.flowable.engine.delegate.JavaDelegate;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.connection.HTTPConnection;
import com.kuliza.lending.common.pojo.HTTPResponse;
import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.common.utils.Constants;
import com.kuliza.lending.common.utils.CustomLogger;
import com.kuliza.lending.common.utils.JobType;
import com.kuliza.lending.common.utils.LogType;

@Service
public class CreditAssessment {

	public void creditAssessmentIntegration(DelegateExecution execution, List<Integer> processIds,
			String userIdKeyInProcess, String applicationIdKeyInProcess, HttpMethod httpMethod, String protocol,
			String url, Integer port, String suburl, String token) throws Exception {

		if (execution == null || processIds == null) {
			throw new IllegalArgumentException(
					"DelegateExecution argument or processIds argument cannot be null if request payload or response object needs to be saved in process instance.");
		} else {
			String userId = "";
			String appId = "";
			String processInstanceId = execution != null ? execution.getProcessInstanceId() : "";
			String losId = execution != null ? execution.getProcessInstanceBusinessKey() : "";

			if (execution != null) {
				if (userIdKeyInProcess != null && !userIdKeyInProcess.equals("")
						&& execution.getVariable(userIdKeyInProcess) != null) {
					userId = execution.getVariable(userIdKeyInProcess).toString();
				} else {
					if (execution.getVariable(Constants.PROCESS_TASKS_ASSIGNEE_KEY) != null) {
						userId = execution.getVariable(Constants.PROCESS_TASKS_ASSIGNEE_KEY).toString();
					}
				}
				if (applicationIdKeyInProcess != null && !applicationIdKeyInProcess.equals("")
						&& execution.getVariable(applicationIdKeyInProcess) != null) {
					appId = execution.getVariable(applicationIdKeyInProcess).toString();
				} else {
					if (execution.getVariable(Constants.APPLICATION_ID_KEY) != null) {
						appId = execution.getVariable(Constants.APPLICATION_ID_KEY).toString();
					} else {
						appId = execution.getProcessInstanceBusinessKey();
					}
				}
			}

			Map<String, Object> processVariables = execution.getVariables();
			JSONObject requestPayload = buildCreditEngineAssessmentRequestPayload(processVariables, processIds);
			CustomLogger.log(userId, processInstanceId, LogType.OUTBOUND_REQUEST_BODY, JobType.OUTBOUND_API, appId,
					losId, requestPayload);
			HTTPResponse response = fireCreditEngine(requestPayload, httpMethod, protocol, url, port, suburl, token);
			CustomLogger.log(userId, processInstanceId, LogType.OUTBOUND_RESPONSE_BODY, JobType.OUTBOUND_API, appId,
					losId, response);
			JSONObject jsonResponse = new JSONObject(response.getResponse().toString());
			Map<String, Object> processVariablesToSave = parseCreditAssessmentResponse(jsonResponse);
			execution.setVariables(processVariablesToSave);

		}

	}

	public JSONObject buildCreditEngineAssessmentRequestPayload(Map<String, Object> processVariables,
			List<Integer> processIds) {
		Map<String, Object> requestParams = new HashMap<String, Object>();
		requestParams.put("productIds", processIds);
		Map<String, Object> inputData = new HashMap<String, Object>();
		for (Map.Entry<String, Object> entry : processVariables.entrySet()) {
			Map<String, Object> variableMap = CommonHelperFunctions.getVariableType(entry);
			inputData.put(variableMap.get("key").toString(), variableMap.get("value"));
		}
		requestParams.put("inputData", inputData);
		JSONObject requestPayload = new JSONObject(requestParams);
		return requestPayload;
	}

	public HTTPResponse fireCreditEngine(JSONObject requestData, HttpMethod httpMethod, String protocol, String url,
			Integer port, String suburl, String token) throws Exception {
		String requestBody = requestData.toString();
		HTTPResponse response = null;
		try {
			response = HTTPConnection.send(protocol, url, port, suburl, requestBody, httpMethod.name(), token);
		} catch (Exception e) {
			response = new HTTPResponse(Constants.FAILURE_CODE, e.toString());
		}
		return response;

	}

	public Map<String, Object> parseCreditAssessmentResponse(JSONObject jsonResponse)
			throws IOException, InterruptedException, Exception {
		Map<String, Object> processVariables = new HashMap<>();
		if (jsonResponse.getInt("status") == Constants.SUCCESS_CODE) {
			JSONArray dataArray = jsonResponse.getJSONArray("data");
			if (dataArray.getJSONObject(0).getInt("status") == 200) {
				JSONObject data = dataArray.getJSONObject(0).getJSONObject("data");
				JSONObject output = data.getJSONObject("output");
				Iterator<String> outputKeys = output.keys();
				while (outputKeys.hasNext()) {
					String key = outputKeys.next();
					if (output.get(key) instanceof JSONObject) {
						JSONObject results = data.getJSONObject(key);
						Iterator<String> resultKeys = results.keys();
						while (resultKeys.hasNext()) {
							String resultKey = resultKeys.next();
							processVariables.put(resultKey, results.get(resultKey));
						}
					} else {
						processVariables.put(key, output.get(key));
					}
					if (output.get(key) instanceof String || output.get(key) instanceof Boolean
							|| output.get(key) instanceof Double) {
						processVariables.put(Constants.CREDIT_ASSESSMENT_DECISION_KEY,
								CommonHelperFunctions.getStringValue(output.get(key).toString().toLowerCase()));
					}
				}
				processVariables.put(Constants.CREDIT_ASSESSMENT_INPUT_KEY, data.getJSONObject("input").toString());
				processVariables.put(Constants.CREDIT_ASSESSMENT_OUTPUT_KEY, data.getJSONObject("output").toString());
				processVariables.put(Constants.CREDIT_ASSESSMENT_STATUS_KEY, "true");
			} else {
				throw new InterruptedException("Error while evaluating Credit Assessment");
			}
		} else {
			throw new InterruptedException("Error response from Credit Assessment");
		}
		return processVariables;
	}

}
