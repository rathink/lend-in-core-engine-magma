package com.kuliza.lending.common.utils;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kuliza.lending.common.pojo.ApiResponse;

public class CommonHelperFunctions {

	public CommonHelperFunctions() {
		throw new UnsupportedOperationException();
	}

	// function to generate error json
	public static List<Map<String, String>> generateErrorResponseData(List<FieldError> errors) throws Exception {
		List<Map<String, String>> allErrorData = new ArrayList<>();
		for (FieldError error : errors) {
			Map<String, String> errorData = new HashMap<>();
			errorData.put("field", error.getField());
			errorData.put("message", error.getDefaultMessage());
			allErrorData.add(errorData);
		}
		return allErrorData;
	}

	// function to generate log string
	public static String generateLogString(String method, int statusCode, String url, String userId, Object data) {
		StringBuilder logString = new StringBuilder();
		logString.append(method.concat(" "));
		logString.append(Integer.toString(statusCode).concat(" "));
		logString.append(url.concat(" "));
		logString.append("UserId : ".concat(userId).concat(" "));
		logString.append("Data : ".concat(data.toString()));
		return logString.toString();
	}

	// function to generate log string
	public static String generateLogString(String method, String url, String userId, Object data) {
		StringBuilder logString = new StringBuilder();
		logString.append(method.concat(" "));
		logString.append(url.concat(" "));
		logString.append("UserId : ".concat(userId).concat(" "));
		logString.append("Data : ".concat(data.toString()));
		return logString.toString();
	}

	// function to generate data for log
	public static Object generateLogData(Object input, Object output) {
		Map<String, Object> logData = new HashMap<>();
		logData.put("input", input);
		logData.put("output", output);
		return logData;
	}

	// function to get stack trace for exception
	public static String getStackTrace(Throwable aThrowable) {
		final Writer result = new StringWriter();
		final PrintWriter printWriter = new PrintWriter(result);
		aThrowable.printStackTrace(printWriter);
		return result.toString();
	}

	public static String getStringValue(Object s) {
		return s != null ? s.toString() : "";
	}

	public static String getStringValueOrEmptyArrayString(Object s) {
		return s != null ? s.toString() : "[]";
	}

	public static String getStringValueOrEmptyMapString(Object s) {
		return s != null ? s.toString() : "{}";
	}

	public static String getStringValueOrDefault(Object s, String defaultString) {
		return s != null ? s.toString() : defaultString;
	}

	public static Long getLongValue(Object s) {
		try {
			return s != null ? (long) Double.parseDouble(s.toString()) : new Long(0);
		} catch (NumberFormatException e) {
			return new Long(0);
		}
	}

	public static Integer getIntegerValue(Object s) {
		try {
			return s != null ? (int) Double.parseDouble(s.toString()) : 0;
		} catch (NumberFormatException e) {
			return 0;
		}
	}

	public static Double getDoubleValue(Object s) {
		try {
			return s != null ? Double.parseDouble(s.toString()) : new Double(0);
		} catch (NumberFormatException e) {
			return new Double(0);
		}

	}

	public static BigDecimal getBigDecimalValue(Object s) {
		try {
			return s != null ? new BigDecimal(s.toString()) : new BigDecimal("0");
		} catch (NumberFormatException e) {
			return new BigDecimal("0");
		}
	}

	public static Boolean getBooleanValue(Object s) {
		return s != null && s.toString().toLowerCase().matches("^(true)|(false)$") ? Boolean.parseBoolean(s.toString())
				: false;
	}

	public static final String generateUUIDTransanctionId() {
		return UUID.randomUUID().toString();
	}

	public static final ZonedDateTime getCurrentDateTime() {
		return ZonedDateTime.now(ZoneOffset.UTC);
	}

	public static final String getCurrentDateInFormat(String format) {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		String strDate = formatter.format(date);
		return strDate;
	}

	public static final String getDateInFormat(Date date, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		String strDate = formatter.format(date);
		return strDate;
	}

	public static final ResponseEntity<Object> buildResponseEntity(ApiResponse apiResponse) {
		return new ResponseEntity<>(apiResponse, HttpStatus.valueOf(apiResponse.getStatus()));
	}

	public static final ResponseEntity<Object> buildResponseEntity(ApiResponse apiResponse, HttpStatus httpStatus) {
		return new ResponseEntity<>(apiResponse, httpStatus);
	}

	public static Map<String, Object> getMap(JSONObject jsonObject, List<String> keys) {

		Map<String, Object> map = new HashMap<>();
		for (String key : keys)
			map.put(key, jsonObject.get(key));
		return map;
	}

	public static boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static boolean isBigInt(String s) {
		try {
			new BigInteger(s);
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static boolean isFloat(String s) {
		try {
			Float.parseFloat(s);
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static boolean isBoolean(String s) {
		if (s.toLowerCase().equals("false") || getStringValue(s).toLowerCase().equals("true")) {
			return true;
		}
		return false;

	}

	// get variable type credit engine
	public static Map<String, Object> getVariableType(Map.Entry<String, Object> entry) {
		Map<String, Object> variableMap = new HashMap<>();
		if (isInteger(getStringValue(entry.getValue()))) {
			variableMap.put("key", entry.getKey() + "-Numerical");
			variableMap.put("value", getIntegerValue(entry.getValue().toString()));
			return variableMap;
		} else if (isBigInt(getStringValue(entry.getValue()))) {
			variableMap.put("key", entry.getKey() + "-Numerical");
			variableMap.put("value", new BigInteger(entry.getValue().toString()));
			return variableMap;
		} else if (isFloat(getStringValue(entry.getValue()))) {
			variableMap.put("key", entry.getKey() + "-Numerical");
			variableMap.put("value", Float.parseFloat(entry.getValue().toString()));
			return variableMap;
		} else if (isBoolean(getStringValue(entry.getValue()))) {
			variableMap.put("key", entry.getKey() + "-Boolean");
			variableMap.put("value", getBooleanValue(entry.getValue()));
			return variableMap;
		} else {
			variableMap.put("key", entry.getKey() + "-String");
			variableMap.put("value", entry.getValue().toString());
			return variableMap;
		}
	}

	// validate if a string is json string
	public static boolean isJSONValid(String jsonInString) {
		try {
			final ObjectMapper mapper = new ObjectMapper();
			mapper.readTree(jsonInString);
			return true;
		} catch (IOException e) {
			return false;
		}
	}

	public static Map<String, Object> jsonStringToMap(String jsonString) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, Object> map = new HashMap<String, Object>();
		try {
			map = mapper.readValue(jsonString, new TypeReference<Map<String, Object>>() {
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return map;
	}

	public static Long getIdFromUserName(String userName) {
		Long id = null;
		id = getLongValue(userName.replaceAll("[^0-9]", ""));
		return id;
	}

	/**
	 * 
	 * @param list      of string
	 * @param seperator like "," ,"@"
	 * @return string wiht seperator
	 */
	public static String getStringFromListWithSeperator(List<String> list, String seperator) {
		StringBuffer buffer = new StringBuffer();
		String defaultSeperator = ",";
		seperator = (seperator != null) ? (seperator.trim().equals("") ? defaultSeperator : seperator)
				: defaultSeperator;
		for (Iterator iterator = list.iterator(); iterator.hasNext();) {
			String string = (String) iterator.next();
			buffer.append(string);
			buffer.append(seperator);
		}
		return (buffer.length() > 0) ? buffer.toString().substring(0, buffer.length() - 1) : buffer.toString();
	}

	public static List<String> GetRolesInList(List<String> rolesList) {
		List<String> roles = new ArrayList<String>();
		for (String roleName : rolesList) {
			if (roleName.startsWith("ROLE_")) {
				roleName = roleName.substring(5);
				roles.add(roleName);
			}
		}
		return roles;
	}

	
	/**
	 * This is an utility method used to check if a given string is not null and
	 * empty
	 * 
	 * @param value
	 * @return true if String is not empty else false
	 */
	public static boolean isNotEmpty(String value) {
		return (value != null) && (!value.trim().equals(""));
	}
}
