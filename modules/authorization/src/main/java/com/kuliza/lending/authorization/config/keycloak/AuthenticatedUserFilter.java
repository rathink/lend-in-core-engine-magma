package com.kuliza.lending.authorization.config.keycloak;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.MDC;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.kuliza.lending.common.utils.CommonHelperFunctions;
import com.kuliza.lending.utils.AuthConstants;

@Component
@Order(Ordered.LOWEST_PRECEDENCE)
public class AuthenticatedUserFilter implements Filter {

	@Override
	public void init(FilterConfig fc) throws ServletException {
		// Just Empty
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		if (SecurityContextHolder.getContext().getAuthentication() instanceof KeyCloakAuthToken) {
			KeyCloakAuthToken token = (KeyCloakAuthToken) SecurityContextHolder.getContext().getAuthentication();
			Collection<GrantedAuthority> authorities = token.getAuthorities();
			List<String> roles = new ArrayList<String>();
			for (GrantedAuthority authority : authorities) {
				roles.add(authority.getAuthority());
			}
			req.setAttribute(AuthConstants.USERNAME, token.getName());
			req.setAttribute(AuthConstants.USER_ROLES, roles);
			req.setAttribute(AuthConstants.USER_ID, token.getUser_Id());

			/**
			 * Changes for getting attributes in logging
			 */

			HttpServletRequest request = (HttpServletRequest) req;
			Principal principal = request.getUserPrincipal();
			if (principal != null) {
				MDC.put(AuthConstants.USERNAME, principal.getName());
				MDC.put(AuthConstants.USER_ROLES, CommonHelperFunctions
						.getStringFromListWithSeperator(CommonHelperFunctions.GetRolesInList(roles), ","));
				MDC.put(AuthConstants.USER_ID, CommonHelperFunctions.getStringValue(token.getUser_Id()));
				MDC.put(AuthConstants.SESSION,
						request.getSession() != null ? CommonHelperFunctions.getStringValue(request.getSession())
								: AuthConstants.NO_SESSION);
			}
		}
		chain.doFilter(req, resp);
	}

	@Override
	public void destroy() {
		// Just Empty
	}

}
