package com.kuliza.lending.authorization.config.keycloak;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import com.kuliza.lending.common.pojo.ApiResponse;

@Component
public class KeyCloakAccessDeniedHandler implements AccessDeniedHandler {

	@Autowired
	@Qualifier("mappingJackson2HttpMessageConverter")
	HttpMessageConverter httpMessageConverter;

	private static final Logger logger = LoggerFactory.getLogger(KeyCloakAccessDeniedHandler.class);

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException exc)
			throws IOException, ServletException {
		logger.debug("Inside Authorization KeyCloakAccessDeniedHandler class!");
		ApiResponse apiResponse = new ApiResponse(HttpStatus.FORBIDDEN, "Access Denied", "User don't have permissions to perform this operation");
		ServerHttpResponse outputMessage = new ServletServerHttpResponse(response);
		outputMessage.setStatusCode(HttpStatus.OK);
		httpMessageConverter.write(apiResponse, null, outputMessage);
	}
}
