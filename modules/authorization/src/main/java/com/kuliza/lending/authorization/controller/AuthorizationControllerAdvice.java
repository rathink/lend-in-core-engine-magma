package com.kuliza.lending.authorization.controller;

import java.util.Map;

import javax.ws.rs.ClientErrorException;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.ServerErrorException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.kuliza.lending.authorization.exception.InternalServerError;
import com.kuliza.lending.authorization.exception.MockException;
import com.kuliza.lending.authorization.exception.UserNotFoundException;
import com.kuliza.lending.common.pojo.ApiResponse;

@RestControllerAdvice
public class AuthorizationControllerAdvice {

	public ApiResponse badRequestHandler(final Throwable throwable) {

		return new ApiResponse(HttpStatus.BAD_REQUEST, throwable.getMessage());
	}

	private ApiResponse errorHandler(final Throwable throwable) {
		return new ApiResponse(HttpStatus.INTERNAL_SERVER_ERROR, throwable.getMessage());
	}

	@ExceptionHandler(InternalServerError.class)
	public ApiResponse internalErrorHandler(final InternalServerError e) {
		return this.errorHandler(e);
	}

	@ExceptionHandler(UserNotFoundException.class)
	public ApiResponse taskNotFoundException(final UserNotFoundException e) {
		return this.badRequestHandler(e);
	}

	@ExceptionHandler(ClientErrorException.class)
	public ApiResponse clientErrorHandler(final ClientErrorException e) {
		if (e instanceof NotAuthorizedException) {
			NotAuthorizedException notAuthorizedException = (NotAuthorizedException) e;
			Map<String, Object> data = notAuthorizedException.getResponse().readEntity(Map.class);
			return new ApiResponse(HttpStatus.valueOf(e.getResponse().getStatus()), e.getMessage(), data);

		}
		return new ApiResponse(HttpStatus.valueOf(e.getResponse().getStatus()), e.getMessage());
	}

	@ExceptionHandler(ServerErrorException.class)
	public ApiResponse serverErrorHandler(final ServerErrorException e) {
		return this.errorHandler(e);
	}

	@ExceptionHandler(MockException.class)
	public ResponseEntity<ApiResponse> mockHandler(final MockException e) {

		return new ResponseEntity<>(
				new ApiResponse(HttpStatus.valueOf(Integer.parseInt(e.getMessage())), e.getMessage(), null),
				HttpStatus.valueOf(Integer.parseInt(e.getMessage())));
	}
	
	@ExceptionHandler({Exception.class})
	  public ResponseEntity<ApiResponse> handleAll(final Exception ex) {
	   return new ResponseEntity<>(
				new ApiResponse(HttpStatus.valueOf(Integer.parseInt(ex.getMessage())), ex.getMessage(), null),
				HttpStatus.valueOf(Integer.parseInt(ex.getMessage())));
	  }
}
