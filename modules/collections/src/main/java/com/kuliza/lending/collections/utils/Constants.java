package com.kuliza.lending.collections.utils;

public class Constants {
    private Constants() {
        throw new UnsupportedOperationException();
    }

    // Controller URLs
    public static final String DECISION_TABLE_URL = "/decision-table/";
    public static final String CREATE_DECISION_TABLE_URL = "create";
    public static final String DEPLOY_DECISION_TABLE_URL = "deploy";
    public static final String FETCH_DECISION_TABLE_URL = "fetch";
    public static final String DELETE_DECISION_TABLE_URL = "delete";

}
