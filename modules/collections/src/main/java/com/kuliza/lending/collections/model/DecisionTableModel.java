package com.kuliza.lending.collections.model;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.kuliza.lending.collections.pojo.DecisionTableModelRepresentation;
import com.kuliza.lending.common.model.BaseModel;



//This is the model to store the decision table, from which it can be deployed to the DMN engine
@Entity
public class DecisionTableModel extends BaseModel {


    private String name;
    private String nameKey;

    @Column(nullable = true)
    private String description;

    private int version;

    //This field (modelEditorJson) stores json for the model which is converted into .dmn
    @Column(columnDefinition="TEXT")
    private String modelEditorJson;

    @Column(nullable = true)
    private String comment;


    public DecisionTableModel() {
        super();
        this.setIsDeleted(false);
    }

    public  DecisionTableModel(DecisionTableModelRepresentation decisionTableModelRepresentation){
        super();
        this.name = decisionTableModelRepresentation.getName();
        this.nameKey = decisionTableModelRepresentation.getKey();
        this.description = decisionTableModelRepresentation.getDescription();
        this.setIsDeleted(false);
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getModelEditorJson() {
        return modelEditorJson;
    }

    public void setModelEditorJson(String modelEditorJson) {
        this.modelEditorJson = modelEditorJson;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getNameKey() {
        return nameKey;
    }

    public void setNameKey(String nameKey) {
        this.nameKey = nameKey;
    }


}

