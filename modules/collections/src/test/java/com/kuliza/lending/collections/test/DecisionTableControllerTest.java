package com.kuliza.lending.collections.test;

import com.kuliza.lending.collections.controllers.DecisionTableController;
import com.kuliza.lending.collections.model.DecisionTableModel;
import com.kuliza.lending.collections.pojo.DecisionTableModelRepresentation;
import com.kuliza.lending.collections.pojo.DecisionTableRequestRepresentation;
import com.kuliza.lending.collections.services.DecisionTableService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static junit.framework.TestCase.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class DecisionTableControllerTest {

    @Mock
    private DecisionTableService decisionTableService;

    @InjectMocks
    private DecisionTableController decisionTableController;


    private DecisionTableModel decisionTableModel = new DecisionTableModel();
    private DecisionTableModelRepresentation decisionTableModelRepresentation = new DecisionTableModelRepresentation();


    ResponseEntity<DecisionTableModel> responseEntityDecisionTable;
    ResponseEntity<DecisionTableModelRepresentation> responseEntityDecisionTableModelRepresentation;

    @Test
    public void createDecisionTableControllerTest() throws Exception {
        decisionTableModelRepresentation.setName("TestDecisionTable");
        decisionTableModelRepresentation.setKey("TestDecisionTableKey");
        decisionTableModel.setName("TestDecisionTable");
        decisionTableModel.setNameKey("TestDecisionTableKey");
        responseEntityDecisionTable = new ResponseEntity<DecisionTableModel>(decisionTableModel, HttpStatus.CREATED);

        Mockito.when(decisionTableService.addDecisionTable(decisionTableModelRepresentation)).thenReturn(decisionTableModel);
        assertEquals(responseEntityDecisionTable, decisionTableController.createAndDeployDecisionTable(decisionTableModelRepresentation));


    }

    @Test
    public void deployDecisionTableControllerTest() throws Exception {
        DecisionTableRequestRepresentation decisionTableRequestRepresentation = new DecisionTableRequestRepresentation();
        decisionTableRequestRepresentation.setDecisionKey("TestDecisionTableKey");
        decisionTableRequestRepresentation.setVersion(2);
        decisionTableModel.setNameKey("TestDecisionTableKey");
        decisionTableModel.setVersion(2);
        responseEntityDecisionTable = new ResponseEntity<DecisionTableModel>(decisionTableModel, HttpStatus.OK);

        Mockito.when(decisionTableService.deployDecisionTable(decisionTableRequestRepresentation.getDecisionKey(),
                decisionTableRequestRepresentation.getVersion())).thenReturn(decisionTableModel);
        assertEquals(responseEntityDecisionTable, decisionTableController.deployDecisionTable(decisionTableRequestRepresentation));


    }

    @Test
    public void fetchDecisionTableControllerTest() throws Exception {

        decisionTableModel.setName("TestDecisionTable");
        decisionTableModel.setNameKey("TestDecisionTableKey");
        DecisionTableModelRepresentation decisionTableModelRepresentation = new DecisionTableModelRepresentation(decisionTableModel);
        responseEntityDecisionTableModelRepresentation = new ResponseEntity<DecisionTableModelRepresentation>(decisionTableModelRepresentation, HttpStatus.OK);
        Mockito.when(decisionTableService.fetchDecisionTableDefinition("TestDecisionTableKey", 2)).thenReturn(decisionTableModelRepresentation);
        assertEquals(responseEntityDecisionTableModelRepresentation, decisionTableController.fetchDecisionTable("TestDecisionTableKey",2));


    }

    @Test
    public void deleteDecisionTableControllerTest() throws Exception {

        DecisionTableRequestRepresentation decisionTableRequestRepresentation = new DecisionTableRequestRepresentation();
        decisionTableRequestRepresentation.setDecisionKey("TestDecisionTableKey");
        decisionTableRequestRepresentation.setVersion(2);
        decisionTableModel.setName("TestDecisionTable");
        decisionTableModel.setNameKey("TestDecisionTableKey");
        responseEntityDecisionTable = new ResponseEntity<DecisionTableModel>(decisionTableModel, HttpStatus.OK);
        Mockito.when(decisionTableService.deleteDecisionTable("TestDecisionTableKey", 2)).thenReturn(decisionTableModel);
        assertEquals(responseEntityDecisionTable, decisionTableController.deleteDecisionTable(decisionTableRequestRepresentation));


    }


}
