package com.kuliza.lending.developer.service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.flowable.engine.HistoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.history.HistoricProcessInstance;
import org.flowable.engine.runtime.Execution;
import org.flowable.engine.runtime.ProcessInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kuliza.lending.common.exception.ProcessInstanceCompletedException;
import com.kuliza.lending.common.exception.ProcessInstanceDoesNotExistException;
import com.kuliza.lending.common.exception.ProcessInstanceNotCompletedException;
import com.kuliza.lending.common.pojo.ApiResponse;
import com.kuliza.lending.common.utils.Constants;

@Service
public class DeveloperJourneyServices {

	private static final Logger logger = LoggerFactory.getLogger(DeveloperJourneyServices.class);

	@Autowired
	private RuntimeService runtimeService;

	@Autowired
	private HistoryService historyService;

	/**
	 * 
	 * This functions provides all the process variables from parent as well as its
	 * child call activiti instances.
	 * 
	 * @param parentProcessInstanceId
	 * @return
	 * @throws NullPointerException
	 * @throws ProcessInstanceCompletedException
	 * @throws ProcessInstanceDoesNotExistException
	 * 
	 * @author Arpit Agrawal
	 */
	public Map<String, Object> getAllProcessVariablesFromRunningParentProcessInstance(String parentProcessInstanceId)
			throws NullPointerException, ProcessInstanceCompletedException, ProcessInstanceDoesNotExistException {

		logger.info("nfo--> Entering getAllProcessVariablesFromRunningParentProcessInstance()");
		Map<String, Object> processVariables = new HashMap<>();
		if (parentProcessInstanceId == null) {
			throw new NullPointerException("parentProcessInstanceId is null");
		}
		if (!parentProcessInstanceId.matches(Constants.ENGINE_TABLES_ID_REGEX)) {
			throw new IllegalArgumentException(
					"parentProcessInstanceId should match regex : " + Constants.ENGINE_TABLES_ID_REGEX);
		} else {
			ProcessInstance parentProcessInstance = runtimeService.createProcessInstanceQuery()
					.processInstanceId(parentProcessInstanceId).includeProcessVariables().singleResult();
			if (parentProcessInstance != null) {
				if (parentProcessInstance.isEnded()) {
					throw new ProcessInstanceCompletedException(parentProcessInstanceId);
				} else {
					if (parentProcessInstance.getProcessVariables().isEmpty()) {
						logger.debug("Parent process variables empty. Only checking for child executions.");
					} else {
						processVariables = parentProcessInstance.getProcessVariables();
					}
					List<Execution> childProcessExecutions = runtimeService.createExecutionQuery()
							.rootProcessInstanceId(parentProcessInstance.getId()).list();
					for (Execution singleExecution : childProcessExecutions) {
						logger.debug("Checking child process instance : " + singleExecution.getProcessInstanceId()
								+ " variable for parent process instance : " + parentProcessInstanceId);
						ProcessInstance childProcessInstance = runtimeService.createProcessInstanceQuery()
								.processInstanceId(singleExecution.getProcessInstanceId()).includeProcessVariables()
								.singleResult();
						processVariables.putAll(childProcessInstance.getProcessVariables());
					}
					processVariables.put("PROC_INST_ID", parentProcessInstance.getId());
					logger.info("<-- Exiting getAllProcessVariablesFromRunningParentProcessInstance()::PROC_INST_ID:"
							+ processVariables.get("PROC_INST_ID"));
					return processVariables;
				}
			} else {
				throw new ProcessInstanceDoesNotExistException(parentProcessInstanceId);
			}

		}
	}

	/**
	 * 
	 * This functions provides all the process variables from process instance which
	 * is ended.
	 * 
	 * @param parentProcessInstanceId
	 * @return Map containing process variables
	 * @throws NullPointerException
	 * @throws IllegalArgumentException
	 * @throws ProcessInstanceDoesNotExistException
	 * @throws ProcessInstanceNotCompletedException
	 * 
	 * @author Arpit Agrawal
	 */
	public Map<String, Object> getAllProcessVariablesFromEndedParentProcessInstance(String parentProcessInstanceId)
			throws NullPointerException, IllegalArgumentException, ProcessInstanceDoesNotExistException,
			ProcessInstanceNotCompletedException {
		logger.info("--> Entering getAllProcessVariablesFromEndedParentProcessInstance()");
		if (parentProcessInstanceId == null) {
			throw new NullPointerException("parentProcessInstanceId is null");
		}
		if (!parentProcessInstanceId.matches(Constants.ENGINE_TABLES_ID_REGEX)) {
			throw new IllegalArgumentException(
					"parentProcessInstanceId should match regex : " + Constants.ENGINE_TABLES_ID_REGEX);
		} else {
			HistoricProcessInstance parentProcessInstance = historyService.createHistoricProcessInstanceQuery()
					.processInstanceId(parentProcessInstanceId).includeProcessVariables().singleResult();
			if (parentProcessInstance != null) {
				if (parentProcessInstance.getEndTime() == null) {
					throw new ProcessInstanceNotCompletedException(parentProcessInstanceId);
				} else {
					Map<String, Object> processVariables = parentProcessInstance.getProcessVariables();
					processVariables.put("PROC_INST_ID", parentProcessInstance.getId());
					logger.info("<-- Exiting getAllProcessVariablesFromEndedParentProcessInstance()::PROC_INST_ID:"
							+ processVariables.get("PROC_INST_ID"));
					return processVariables;
				}
			} else {
				throw new ProcessInstanceDoesNotExistException(parentProcessInstanceId);
			}

		}
	}

	public ApiResponse getAllProcessVariables(String processInstanceId) throws Exception {
		logger.info("--> Entering getAllProcessVariables()");
		HistoricProcessInstance processInstance = historyService.createHistoricProcessInstanceQuery()
				.processInstanceId(processInstanceId).includeProcessVariables().singleResult();
		Map<String, Object> processVariables = new HashMap<>();
		if (processInstance == null) {
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
		} else {
			try {
				logger.debug("fetching all process variables for process Instance:"+processInstanceId);
				processVariables = getAllProcessVariablesFromEndedParentProcessInstance(processInstanceId);
			} catch (ProcessInstanceNotCompletedException e) {
				logger.debug(
						"Given process instance " + processInstanceId + " not completed. Checking in runtime service.");
				processVariables = getAllProcessVariablesFromRunningParentProcessInstance(processInstanceId);
			}
			logger.info("<-- Exiting getAllProcessVariables()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, processVariables);
		}
	}

	public ApiResponse getAllProcessVariablesFromUserId(String userId) throws Exception {
		logger.info("--> Entering getAllProcessVariablesFromUserId()::userid:"+userId);
		List<HistoricProcessInstance> processInstances = historyService.createHistoricProcessInstanceQuery()
				.startedBy(userId).orderByProcessInstanceStartTime().desc().includeProcessVariables().list();
		Map<String, Object> processVariables = new HashMap<>();
		if (processInstances == null || processInstances.isEmpty()) {
			return new ApiResponse(HttpStatus.BAD_REQUEST, Constants.NO_PROCESS_INSTANCE_FOR_MOBILE_MESSAGE);
		} else {
			try {
				processVariables = getAllProcessVariablesFromEndedParentProcessInstance(
						processInstances.get(0).getId());
			} catch (ProcessInstanceNotCompletedException e) {
				logger.debug("Given user id corresponding to process instance " + processInstances.get(0).getId()
						+ " not completed. Checking in runtime service.");
				processVariables = getAllProcessVariablesFromRunningParentProcessInstance(
						processInstances.get(0).getId());
			}
			logger.info("<-- Exiting getAllProcessVariablesFromUserId()");
			return new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE, processVariables);
		}
	}

	public ApiResponse killAllProcessInstances(String procDefinitionKey) {
		logger.info("--> Entering killAllProcessInstances()::procDefinitionKey:"+procDefinitionKey);
		ApiResponse response = null;
		if (procDefinitionKey == null) {
			logger.debug("procDefinitionKey is null:: killing all active process Instances");
			List<ProcessInstance> allProcessInstance = runtimeService.createProcessInstanceQuery().active().list();
			for (ProcessInstance singleProcessInstance : allProcessInstance) {
				logger.debug("Deleting process with processInstanceId:"+singleProcessInstance.getProcessInstanceId());
				runtimeService.deleteProcessInstance(singleProcessInstance.getProcessInstanceId(),
						Constants.DELETED_BY_DEV_MESSAGE);
			}
			logger.info("Successfully Deleted");
			response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
		} else {
			logger.debug("procDefinitionKey is "+procDefinitionKey+":: killing all active process Instances for given key");
			List<ProcessInstance> allProcessInstance = runtimeService.createProcessInstanceQuery()
					.processDefinitionKey(procDefinitionKey).active().list();
			if (allProcessInstance == null) {
				logger.debug("No instances found or invalid procDefinitionKey:"+procDefinitionKey);
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_DEFINITION_KEY_MESSAGE);
			} else {
				Set<String> killedProcessInstance = new HashSet<>();
				for (ProcessInstance singleProcessInstance : allProcessInstance) {
					System.out.println(singleProcessInstance.getProcessInstanceId());
					runtimeService.deleteProcessInstance(singleProcessInstance.getProcessInstanceId(),
							Constants.DELETED_BY_DEV_MESSAGE);
					killedProcessInstance.add(singleProcessInstance.getProcessInstanceId());
					List<Execution> allChildMainExecutions = runtimeService.createExecutionQuery()
							.rootProcessInstanceId(singleProcessInstance.getProcessInstanceId()).list();
					for (Execution singleExecution : allChildMainExecutions) {
						if (!killedProcessInstance.contains(singleExecution.getProcessInstanceId())) {
							System.out.println(singleExecution.getProcessInstanceId());
							runtimeService.deleteProcessInstance(singleExecution.getProcessInstanceId(),
									Constants.DELETED_BY_DEV_MESSAGE);
							killedProcessInstance.add(singleExecution.getProcessInstanceId());
						}

					}
				}
				logger.debug("killedProcessInstance:"+killedProcessInstance);
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
			}
		}
		logger.info("<-- Exiting killAllProcessInstances()");
		return response;
	}

	public ApiResponse killSingleProcessInstance(String processInstanceId) {
		logger.info("--> Entering killSingleProcessInstance()");
		ApiResponse response = null;
		if (processInstanceId == "" || processInstanceId == null) {
			response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
		} else {
			logger.debug("killing processInstance with Id:"+processInstanceId);
			ProcessInstance singleProcessInstance = runtimeService.createProcessInstanceQuery()
					.processInstanceId(processInstanceId).active().singleResult();
			if (singleProcessInstance == null) {
				response = new ApiResponse(HttpStatus.BAD_REQUEST, Constants.INVALID_PROCESS_INSTANCE_ID_MESSAGE);
			} else {
				List<Execution> allChildMainExecutions = runtimeService.createExecutionQuery()
						.rootProcessInstanceId(processInstanceId).list();
				Set<String> killedProcessInstance = new HashSet<>();
				for (Execution singleExecution : allChildMainExecutions) {
					if (!killedProcessInstance.contains(singleExecution.getProcessInstanceId())) {
						logger.debug("deleting process with ProcessInstanceId: "+singleExecution.getProcessInstanceId());
						runtimeService.deleteProcessInstance(singleExecution.getProcessInstanceId(),
								Constants.DELETED_BY_DEV_MESSAGE);
						killedProcessInstance.add(singleExecution.getProcessInstanceId());
					}

				}
				response = new ApiResponse(HttpStatus.OK, Constants.SUCCESS_MESSAGE);
			}
		}
		logger.info("<-- Exiting killSingleProcessInstance()");
		return response;
	}

}
